#pragma once

#include <thread>
#include <vector>
#include <mutex>
#include <condition_variable>

using namespace std;

// queue with capacity of one task (because there is no reason to have more for this project)
// if it would be necessary we could replace it with normal queue
class TaskQueue
{
	function<void()> _queue;

public:
	void enqueue(function<void()> task)
	{
		if (!isEmpty()) throw exception("Task queue overflow.");

		_queue = task;
	}
	function<void()> pop()
	{
		if (isEmpty()) throw exception("No items in Task queue.");

		auto task = _queue;
		_queue = nullptr;

		return task;
	}
	bool isEmpty()
	{
		return _queue == nullptr;
	}
};

// simple thread pool
class MyThreadPool
{
	vector<thread> _threads;
	mutex _m;
	condition_variable _cv;
	TaskQueue _queue;
	size_t _activeTasks = 0;
	bool _shutdownRequested = false;

public:
	MyThreadPool(size_t threadsCount = thread::hardware_concurrency())
	{
		for (size_t i = 0; i < threadsCount; i++)
		{
			_threads.push_back(thread([this]
			{
				while (true)
				{
					function<void()> task;

					// wait for a new task or stop request
					// extract new task and remove it from the "queue"
					{
						unique_lock<mutex> lock(_m);
						_cv.wait(lock, [this] 
						{
							return _shutdownRequested || !_queue.isEmpty(); 
						});

						if (_shutdownRequested)
							break;

						task = _queue.pop();

						_activeTasks++;

						// end of lock(_m);
					}

					_cv.notify_all();

					try
					{
						task();
					}
					catch (...)
					{
						// task should handle exceptions itself.
					}

					{
						unique_lock<mutex> lock(_m);
						_activeTasks--;

						// end of lock(_m);
					}

					_cv.notify_all();
				}
			}));
		}
	}
	~MyThreadPool()
	{
		shutdown();
	}

	void addTask(function<void()> task)
	{
		{
			unique_lock<mutex> lock(_m);
			_cv.wait(lock, [this]
			{
				return _shutdownRequested || _queue.isEmpty();
			});

			// don't accept new tasks if shutdown was requested			
			if (_shutdownRequested)
				return;

			_queue.enqueue(task);

			// end of lock(_m);
		}

		_cv.notify_one(); // wake up one thread
	}
	void waitComplete()
	{
		unique_lock<mutex> lock(_m);
		_cv.wait(lock, [this]
		{
			return _shutdownRequested || _activeTasks == 0 && _queue.isEmpty();
		});

		// end of lock(_m);
	}
	void shutdown()
	{
		{
			unique_lock<mutex> lock(_m);
			_shutdownRequested = true;

			// end of lock(_m);
		}

		_cv.notify_all();

		for (auto& t : _threads)
		{
			if (t.joinable())
			{
				t.join();
			}
		}
	}
};
