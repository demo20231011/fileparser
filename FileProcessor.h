#pragma once

#include <fstream>
#include <filesystem>
#include <algorithm>
#include <thread>
#include "MyTokenizer.h"
#include "MyThreadPool.h"
#include "ErrHandler.h"

using namespace std;
using namespace std::experimental::filesystem;

// multithreaded file processor
class FileProcessor
{
	ofstream _outFile;
	mutex _outFileMutex;

public:
	void processDirectory(string directoryName, string outFileName)
	{
		auto baseDir = system_complete(path(directoryName));
		// STL doesn't have normalize(), so the path folder1/folder2/../file.txt is not converted to folder1/file.txt

		if (!exists(baseDir))
			throw "Directory not found: " + baseDir.string();

		// create new file for result (or rewrite existing)
		_outFile.open(outFileName);
		if (!_outFile)
			throw "Can not create output file: " + outFileName;

		MyThreadPool pool; // create and start a thread pool with default number of threads

		for_each(directory_iterator(baseDir), directory_iterator(), [&pool, this](const directory_entry& e)
		{
			if (is_regular_file(e.status()))
			{
				// NOTE: e must be passed by value here, since it goes to another thread
				pool.addTask([e, this]
				{
					invokeWithErrHandler([e, this]
					{
						processFile(e.path());
					});
				});
			}
		});

		pool.waitComplete();

		// thread pool shuts down here
	}

private:
	struct FileData
	{
		string lineToParse;
		vector<string> delims;
	};

	void processFile(path filePath)
	{
		auto& fileData = readFileData(filePath);

		MyTokenizer tokenizer(fileData.lineToParse, fileData.delims);

		writeToOutFile(tokenizer, filePath.filename().string());
	}
	void writeToOutFile(MyTokenizer& tokenizer, string fileName)
	{
		unique_lock<mutex> lock(_outFileMutex);

		_outFile << "[��� ����� " << fileName << "]:" << endl;

		for_each(begin(tokenizer), end(tokenizer), [this](string& item)
		{
			_outFile << item << endl;
		});

		// end of  lock(_outFileMutex);
	}
	FileData readFileData(path filePath)
	{
		ifstream f(filePath);
		if (!f)
			throw "Can not open file: " + filePath.string();

		// read first line
		string lineToParse;
		getline(f, lineToParse);

		// read delimiters
		vector<string> delims;
		string delim;
		while (getline(f, delim))
		{
			delims.push_back(delim);
		}

		if (f.bad())
			throw "Reading from file failed: " + filePath.string();

		return FileData { lineToParse, delims };
	}
};
