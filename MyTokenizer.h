#pragma once

#include <vector>
#include <algorithm>

using namespace std;

// The tokenizer for splitting the input string using a list of delimiters.
// A delimiter must be a non empty string. The length of a delimiter could be more than one character.
// If more then one delimiter is found in the same position the longest is chosen.
class MyTokenizer
{
	string _str;
	vector<string> _delims;

public:
	class Iterator : public iterator<input_iterator_tag, Iterator>
	{
		string _str;
		vector<string> _delims;
		string::size_type _nextPos = string::npos;
		string _chunk;

	public:
		// this constructor can be used to create the "end" iterator
		Iterator()
		{
		}
		Iterator(string str, vector<string> delims)
		{
			auto emptyDelimiterFound = any_of(delims.begin(), delims.end(), [](string& s)
			{
				return s.empty();
			});
			if (emptyDelimiterFound) throw exception("A delimiter may not be empty.");

			_str = str;
			_delims = delims;

			// re-order delimiters by descending length to give the priority to the longer ones 
			sort(_delims.begin(), _delims.end(), [](string& s1, string& s2)
			{
				return s2.length() < s1.length();
			});

			_nextPos = 0;

			this->operator++(); // do the first iteration
		}
		bool operator !=(Iterator other)
		{
			return _nextPos != other._nextPos;
		}
		bool operator ==(Iterator other)
		{
			return !(*this != other);
		}
		void operator ++()
		{
			if (_nextPos >= _str.length())
			{
				_nextPos = string::npos;
			}
			else
			{
				// find a delimiter with a minimal position in _str, larger or equal to _nextPos

				auto indexFound = string::npos;
				string::size_type delimLen = 0;

				for_each(_delims.begin(), _delims.end(), [this, &indexFound, &delimLen](string& delim)
				{
					auto indexFound2 = _str.find(delim, _nextPos);
					if (indexFound2 != string::npos)
					{
						if (indexFound == string::npos || indexFound2 < indexFound)
						{
							indexFound = indexFound2;
							delimLen = delim.length();
						}
					}
				});

				auto chunkStart = _nextPos;
				auto chunkEnd = indexFound == string::npos ? _str.length() : indexFound;

				_chunk = _str.substr(chunkStart, chunkEnd - chunkStart);

				_nextPos = chunkEnd + delimLen;
			}
		}
		string operator *()
		{
			return _chunk;
		}
	};

	MyTokenizer(string str, vector<string> delims) : _str(str), _delims(delims)
	{
	}
	Iterator begin()
	{
		return Iterator(_str, _delims);
	}
	Iterator end()
	{
		return Iterator();
	}
};
