#pragma once

#include <iostream>
#include <string>
#include <exception>
#include <functional>
#include <mutex>

using namespace std;

mutex _cerrMutex;

void invokeWithErrHandler(function<void()> action)
{
	try
	{
		action();
	}
	catch (const char* msg)
	{
		unique_lock<mutex> lock(_cerrMutex);
		cerr << msg << endl;
	}
	catch (string& msg)
	{
		unique_lock<mutex> lock(_cerrMutex);
		cerr << msg << endl;
	}
	catch (exception& x)
	{
		unique_lock<mutex> lock(_cerrMutex);
		cerr << "Exception (" << typeid(x).name() << "): " << x.what() << endl;
	}
	catch (...)
	{
		unique_lock<mutex> lock(_cerrMutex);
		cerr << "General failure" << endl;
	}
}
