#include <iostream>
#include <string>
#include "FileProcessor.h"
#include "ErrHandler.h"

using namespace std;

void main(int argc, char *argv[])
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); // to trace mem-leaks

	invokeWithErrHandler([&]
	{
		if (argc < 3) throw "Not enough arguments";

		string directoryName = argv[1];
		string outFileName = argv[2];

		FileProcessor fp;
		fp.processDirectory(directoryName, outFileName);
	});
}
